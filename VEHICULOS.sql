﻿-- Table: public.vehiculo


CREATE TABLE public.vehiculo
(
  id integer NOT NULL,
  tipo character varying(1000),
  nombre character varying(1000),
  CONSTRAINT pk_vehiculo PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.vehiculo
  OWNER TO postgres;

-- Table: public.registro


CREATE TABLE public.registro
(
  id_registro SERIAL NOT NULL,
  fecha character varying(20),
  hora character varying(20),
  pos_x integer NOT NULL,
  pos_y integer NOT NULL,
  pos_z integer NOT NULL,	
  id_vehiculo integer NOT NULL references public.vehiculo(id),
  CONSTRAINT pk_registro PRIMARY KEY (id_registro)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.registro
  OWNER TO postgres;
