/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import vehiculos.service.RegistroService;
import vehiculos.modelos.registro;

/**
 *
 * @author modesto
 */
@Path("/registros")
@RequestScoped
public class RegistroREST {
  @Inject
    private Logger log;

    @Inject
    RegistroService registroService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<registro> listar() {
         log.info("Retornando lista de registros");
        return registroService.seleccionar();
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public registro obtenerPorId(@PathParam("id") long id) {
        log.info("Registro obteniendose Por Id " + id + "");
        registro p = registroService.seleccionarPorID(id);
        if (p == null) {
        	log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtener Por Id " + id + " encontrada: ");
        return p;
    }

    @GET
    @Path("/id")
    @Produces(MediaType.APPLICATION_JSON)
    public registro obtenerPorIdQuery(@QueryParam("id") long id) {
        log.info("Registro obteniendose Por Id " + id + "");
        registro p = registroService.seleccionarPorID(id);
        if (p == null) {
        	log.info("obtenerPorIdQ " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: ");
        return p;
    }

    
    
    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
     public List<registro> crear(registro p) {
        

        Response.ResponseBuilder builder = null;
            List<registro> lista = null;
        try {
            registroService.crear(p);
            // Create an "ok" response
            
            //builder = Response.ok();
            
            builder = Response.status(201).entity("vehiculo creada exitosamente");
            lista = registroService.seleccionar();
            
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return lista;
    }

   @DELETE
   @Path("/{id_registro}")
   public Response borrar(@PathParam("id_registro") long id_registro)
   {      
	   Response.ResponseBuilder builder = null;
	   try {
		   
		   if(registroService.seleccionarPorID(id_registro) == null) {
			   
			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("registro no existe.");
		   }else {
			   registroService.borrar(id_registro);
			   builder = Response.status(202).entity("registro borrada exitosamente.");			   
		   }
		   

		   
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
   }   
}
