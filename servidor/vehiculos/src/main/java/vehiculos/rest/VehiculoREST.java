/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import vehiculos.modelos.PeticionDistancia;
import vehiculos.modelos.RespuestaDistancia;
import vehiculos.modelos.registro;
import vehiculos.modelos.vehiculo;
import vehiculos.service.RegistroService;
import vehiculos.service.VehiculoService;

/**
 *
 * @author modesto
 */
@Path("/vehiculos")
@RequestScoped
public class VehiculoREST {
    
    @Inject
    private Logger log;

    @Inject
    VehiculoService vehiculoService;
    
    @Inject
    RegistroService registroService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<vehiculo> listar() {
        log.info("Retornando lista de vehiculos");
        return vehiculoService.seleccionar();
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public vehiculo obtenerPorId(@PathParam("id") long id) {
        vehiculo p = vehiculoService.seleccionarPorID(id);
        if (p == null) {
        	log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + p.getNombre());
        return p;
    }

    @GET
    @Path("/id")
    @Produces(MediaType.APPLICATION_JSON)
    public vehiculo obtenerPorIdQuery(@QueryParam("id") long id) {
        vehiculo p = vehiculoService.seleccionarPorID(id);
        if (p == null) {
        	log.info("obtenerPorId " + id + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + id + " encontrada: " + p.getNombre()+ "tipo: "+ p.getTipo());
        return p;
    }

    @POST
    @Path("/distancia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<RespuestaDistancia> Distancia(PeticionDistancia D) {
        RespuestaDistancia p;
        vehiculo aux;
        log.info("Buscando coincidencias con distancia" +D.getDistancia()+"coordenadas:"+D.getX()+" "+D.getY()+" "+D.getZ());
        List<RespuestaDistancia> lista = new ArrayList<RespuestaDistancia>();
        List<registro> r = registroService.distancia(D);
        for (int i = 0; i < r.size(); i++) {
            aux = vehiculoService.seleccionarPorID(r.get(i).getId_vehiculo()) ;
            if (aux == null) {
        	log.info("No se pudo obtener el Vehiculo " + r.get(i).getId_vehiculo());
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }else{
                log.info("Vehiculo encontrado a distancia dada");
                p  = new RespuestaDistancia(aux,r.get(i).getHora(),r.get(i).getFecha());
                lista.add(p);
            }
        }
        return lista;
    }
    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
  /*  @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(vehiculo p) {

        Response.ResponseBuilder builder = null;

        try {
            vehiculoService.crear(p);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("vehiculo creada exitosamente");
            
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }*/
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<vehiculo> crear(vehiculo p) {
        

        Response.ResponseBuilder builder = null;
            List<vehiculo> lista = null;
        try {
            vehiculoService.crear(p);
            // Create an "ok" response
            
            //builder = Response.ok();
            
            builder = Response.status(201).entity("vehiculo creada exitosamente");
            lista = vehiculoService.seleccionar();
            
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return lista;
    }
   @DELETE
   @Path("/{id}")
   public Response borrar(@PathParam("id") long id)
   {      
	   Response.ResponseBuilder builder = null;
	   try {
		   
		   if(vehiculoService.seleccionarPorID(id) == null) {
			   
			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("vehiculo no existe.");
		   }else {
			   vehiculoService.borrar(id);
			   builder = Response.status(202).entity("vehiculo borrada exitosamente.");			   
		   }
		   

		   
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
   }
   
}
