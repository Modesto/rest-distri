/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.service;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import vehiculos.dao.vehiculosDAO;
import vehiculos.modelos.vehiculo;
/**
 *
 * @author modesto
 */
@Stateless
public class VehiculoService {
     @Inject
    private Logger log;

    @Inject
    private vehiculosDAO dao;

    public void crear(vehiculo p) throws Exception {
        log.info("Registrando Vehiculo: " + p.getNombre() + " " + p.getTipo());
        try {
        	dao.insertar(p);
        }catch(Exception e) {
        	log.severe("ERROR al registrar: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Persona creada con éxito: " + p.getNombre() + " " + p.getTipo() );
    }
    
    public List<vehiculo> seleccionar() {
    	return dao.seleccionar();
    }
    
    public vehiculo seleccionarPorID(long ID) {
    	return dao.seleccionarPorID(ID);
    }
    
    public long borrar(long id) throws Exception {
    	return dao.borrar(id);
    }
}
