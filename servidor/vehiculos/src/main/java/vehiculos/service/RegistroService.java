/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.service;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import vehiculos.dao.registrosDAO;
import vehiculos.modelos.PeticionDistancia;
import vehiculos.modelos.registro;

/**
 *
 * @author modesto
 */
@Stateless
public class RegistroService {
       @Inject
    private Logger log;

    @Inject
    private registrosDAO dao;

    public void crear(registro p) throws Exception {
        log.info("Registrando posicion del Vehiculo: "+ p.getId_vehiculo() +" en la posicion" + p.getPos_x() + " " + p.getPos_y()+ " " + p.getPos_z());
        try {
        	dao.insertar(p);
        }catch(Exception e) {
        	log.severe("ERROR al registrar: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Registro de posicion creada con éxito: " + p.getId_vehiculo() +" en la posicion" + p.getPos_x() + " " + p.getPos_y()+ " " + p.getPos_z());
    }
    
    public List<registro> seleccionar() {
    	return dao.seleccionar();
    }
    public List<registro> distancia(PeticionDistancia D) {
    	return dao.seleccionarDistancia(D);
    }
    
    public registro seleccionarPorID(long ID) {
    	return dao.seleccionarPorID(ID);
    }
    
    public long borrar(long id) throws Exception {
    	return dao.borrar(id);
    }
}
