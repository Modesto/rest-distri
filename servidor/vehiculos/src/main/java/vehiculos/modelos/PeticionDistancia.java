/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.modelos;

/**
 *
 * @author modesto
 */
public class PeticionDistancia {
    double distancia;
    int x;
    int y;
    int z;

    public PeticionDistancia() {
    }

    public PeticionDistancia(double distancia, int x, int y, int z) {
        this.distancia = distancia;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public double getDistancia() {
        return distancia;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
    
}
