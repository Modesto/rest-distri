/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.modelos;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author modesto
 */

@SuppressWarnings("serial")
@XmlRootElement
public class registro implements Serializable{
    Long id_registro;
    Long id_vehiculo;
    int pos_x;
    int pos_y;
    int pos_z;
    String fecha;
    String hora;

    public registro() {
    }

    public registro(Long id_registro, Long id_vehiculo, int pos_x, int pos_y, int pos_z, String fecha, String hora) {
        this.id_registro = id_registro;
        this.id_vehiculo = id_vehiculo;
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.pos_z = pos_z;
        this.fecha = fecha;
        this.hora = hora;
    }

    public Long getId_registro() {
        return id_registro;
    }

    public Long getId_vehiculo() {
        return id_vehiculo;
    }

    public int getPos_x() {
        return pos_x;
    }

    public int getPos_y() {
        return pos_y;
    }

    public int getPos_z() {
        return pos_z;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setId_registro(Long id_registro) {
        this.id_registro = id_registro;
    }

    public void setId_vehiculo(Long id_vehiculo) {
        this.id_vehiculo = id_vehiculo;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    public void setPos_z(int pos_z) {
        this.pos_z = pos_z;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
    
        
}
