/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos.modelos;

/**
 *
 * @author modesto
 */
public class RespuestaDistancia {
    vehiculo movil;
    String hora;
    String fecha;

    public RespuestaDistancia() {
    }

    public RespuestaDistancia(vehiculo movil, String hora, String fecha) {
        this.movil = movil;
        this.hora = hora;
        this.fecha = fecha;
    }

    public void setMovil(vehiculo movil) {
        this.movil = movil;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    public vehiculo getMovil() {
        return movil;
    }

    public String getHora() {
        return hora;
    }

    public String getFecha() {
        return fecha;
    }
    
}
