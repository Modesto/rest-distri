
package vehiculos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import vehiculos.modelos.PeticionDistancia;
import vehiculos.modelos.registro;
/**
 *
 * @author modesto
 */
@Stateless
public class registrosDAO {
      @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */
	public List<registro> seleccionar() {
		String query = "SELECT id_registro, fecha, hora, pos_x, pos_y, pos_z, id_vehiculo FROM registro ORDER BY id_registro";
		
		List<registro> lista = new ArrayList<registro>();
		
		Connection conn = null; 
        try 
        {
        	conn = Conexion.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		registro p = new registro();
        		p.setId_registro(rs.getLong(1));
        		p.setFecha(rs.getString(2));
        		p.setHora(rs.getString(3));
                        p.setPos_x(rs.getInt(4));
                        p.setPos_y(rs.getInt(5));
                        p.setPos_z(rs.getInt(6));
                        p.setId_vehiculo(rs.getLong(7));
        		
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
        public static double calcularDistancia(int x1, int y1, int z1, int x2, int y2, int z2){
            
                double distancia;
                distancia = Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
                return distancia;
        }
        public List<registro> seleccionarDistancia(PeticionDistancia D) {
		String query = "SELECT id_registro, fecha, hora, pos_x, pos_y, pos_z, id_vehiculo FROM registro ORDER BY id_registro";
		
		List<registro> lista = new ArrayList<registro>();
		
		Connection conn = null; 
        try 
        {
        	conn = Conexion.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		registro p = new registro();
        		p.setId_registro(rs.getLong(1));
        		p.setFecha(rs.getString(2));
        		p.setHora(rs.getString(3));
                        p.setPos_x(rs.getInt(4));
                        p.setPos_y(rs.getInt(5));
                        p.setPos_z(rs.getInt(6));
                        p.setId_vehiculo(rs.getLong(7));
                        double calculado = calcularDistancia(D.getX(), D.getY(), D.getZ(), p.getPos_x(), p.getPos_y(), p.getPos_z());
                        log.info("Distancia - Procesando registro " + p.getId_registro() +"VehiculoID: "+p.getId_vehiculo()+"Distancia calculada:"+ calculado);
        		if( calculado <= D.getDistancia()){
                                lista.add(p);
                        }
        		
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public registro seleccionarPorID(long id) {
		String SQL = "SELECT fecha, hora, pos_x, pos_y, pos_z, id_vehiculo FROM registro WHERE id_registro = ? ";
		
		registro p = null;
		
		Connection conn = null; 
        try 
        {
        	conn = Conexion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, id);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		p = new registro();
        		p.setFecha(rs.getDate(1).toString());
        		p.setHora(rs.getTime(2).toString());
                        p.setPos_x(rs.getInt(3));
                        p.setPos_y(rs.getInt(4));
                        p.setPos_z(rs.getInt(5));
                        p.setId_vehiculo(rs.getLong(6));
                        p.setId_registro(id);
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return p;

	}

	
	
    public long insertar(registro p) throws SQLException {

        String SQL = "INSERT INTO registro(fecha, hora, pos_x, pos_y, pos_z, id_vehiculo) "
                + "VALUES(?,?,?,?,?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Conexion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, p.getFecha());
            pstmt.setString(2, p.getHora());
            pstmt.setInt(3, p.getPos_x());
            pstmt.setInt(4, p.getPos_y());
            pstmt.setInt(5, p.getPos_z());
            pstmt.setLong(6, p.getId_vehiculo());
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    	
    	
    }
	

    public long actualizar(registro p) throws SQLException {

        String SQL = "UPDATE registro SET fecha = ?, hora = ?, pos_x = ?, pos_y = ?, pos_z = ?, id_vehiculo = ? WHERE id_registro = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Conexion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, p.getFecha());
            pstmt.setString(2, p.getHora());
            pstmt.setInt(3, p.getPos_x());
            pstmt.setInt(4, p.getPos_y());
            pstmt.setInt(5, p.getPos_z());
            pstmt.setLong(6, p.getId_vehiculo());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    public long borrar(long IdR) throws SQLException {

        String SQL = "DELETE FROM registro WHERE id_registro = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Conexion.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, IdR);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    
}
