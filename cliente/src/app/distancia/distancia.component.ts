import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {DistanciaResp} from '../DistanciaResp';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Component({
  selector: 'app-distancia',
  templateUrl: './distancia.component.html',
  styleUrls: ['./distancia.component.css']
})
export class DistanciaComponent implements OnInit {

  constructor(private http:HttpClient) {
    
  }

  ngOnInit() {
  }
  distancia: number = 0;
  pos_x: number= 0;
  pos_y: number= 0;
  pos_z: number= 0;
  listaDistancias = null;

  RequestDistbutton(){
    this.http.post('http://localhost:8080/api/rest/vehiculos/distancia',{distancia:this.distancia,
    pos_x:this.pos_x,pos_y:this.pos_y,pos_z:this.pos_z}, httpOptions).subscribe((data: Array<DistanciaResp>)=> this.listaDistancias=data);
  }
}
