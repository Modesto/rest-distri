import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';



import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VehiculosComponent } from './vehiculos/vehiculos.component';
import { RegistrosComponent } from './registros/registros.component';
import { DistanciaComponent } from './distancia/distancia.component';


const appRoutes: Routes = [
  { path: 'vehiculos', component: VehiculosComponent },
  { path: 'registros', component: RegistrosComponent },
  { path: 'distancias', component: DistanciaComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    VehiculosComponent,
    RegistrosComponent,
    DistanciaComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // debugg opcion
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
