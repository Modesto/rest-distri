import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Registros} from '../registros'
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Component({
  selector: 'app-registros',
  templateUrl: './registros.component.html',
  styleUrls: ['./registros.component.css']
})
export class RegistrosComponent implements OnInit {

  constructor(private http:HttpClient) {
    
  }

  ngOnInit() {
    this.getAllRegistros();
  }
  listaRegistros = null;
  id_registro: number = 0;
  id_vehiculo: string="";
  pos_x: number= 0;
  pos_y: number= 0;
  pos_z: number= 0;
  fecha: string= "";
  hora: string= "";
  getAllRegistros() {
    return this.http.get('http://localhost:8080/api/rest/registros').subscribe((data: Array<Registros>)=> this.listaRegistros=data);
 }
 addRegistro(){
  this.http.post('http://localhost:8080/api/rest/registros',{fecha:this.fecha,hora:this.hora,pos_x:this.pos_x,pos_y:this.pos_y,pos_z:this.pos_z,id_vehiculo:this.id_vehiculo}, httpOptions).subscribe(
    (data: Array<Registros>)=> this.listaRegistros=data); 
  
}
addRbutton(){
  this.addRegistro();
  this.id_registro = 0;
  this.id_vehiculo="";
  this.pos_x= 0;
  this.pos_y= 0;
  this.pos_z= 0;
  this.fecha= "";
  this.hora="";

}

}
