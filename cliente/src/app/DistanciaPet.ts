export interface DistancaiPet {
    distancia: number;
    pos_x: number;
    pos_y: number;
    pos_z: number;
}