import {Vehiculos} from './vehiculos'
export interface DistanciaResp {
    movil: Vehiculos;
    hora: string;
    fecha: string;
}