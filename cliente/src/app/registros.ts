export interface Registros {
    id_registro: number;
    id_vehiculo: string;
    pos_x: number;
    pox_y: number;
    pos_z: number;
    fecha: string;
    hora: string;
}