import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Vehiculos} from '../vehiculos'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {

  constructor(private http:HttpClient, private changeDetection: ChangeDetectorRef) {
    
  }

  ngOnInit() {
    this.getAllVehiculos();
  }



  listaVehiculos = null;
  nombre:string="";
  tipo:string="";
  id:number=0;


  getAllVehiculos() {
    return this.http.get('http://localhost:8080/api/rest/vehiculos').subscribe((data: Array<Vehiculos>)=> this.listaVehiculos=data);
  }
  addVeh(){
    this.http.post('http://localhost:8080/api/rest/vehiculos',{id:this.id,tipo:this.tipo,nombre:this.nombre}, httpOptions).subscribe((data: Array<Vehiculos>)=> this.listaVehiculos=data); 
  }
  addVehiculo(){
    this.addVeh();
    this.nombre = "";
    this.id = 0;
    this.tipo = "";
  }
}
